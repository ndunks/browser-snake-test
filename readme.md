# Simple Snake Game for Browser using TypeScript

## How to Play

This repo contain pre-transpiled TypeScript, so just click 'index.html' and go.

## How to Build

- Run `yarn` to install depencies
- Run `yarn server` to transpile TypeScript in watch-mode

## Screenshot

![Screenshot](screenshot.png)
