
declare interface Window {
    snake: SnakeGame
}
declare var snake: SnakeGame

enum Direction {
    ArrowRight,
    ArrowDown,
    ArrowLeft,
    ArrowUp
}

type ImagePart = [HTMLImageElement, number/* x */, number/* y */, number/* width */, number /* height */];

interface ImageParts {
    head: ImagePart,
    headEat: ImagePart,
    meal: ImagePart,
    body: ImagePart[],
    tail: ImagePart[],
}

abstract class Drawable {
    abstract image: ImagePart
    constructor(
        public x: number,
        public y: number,
        protected w: number,
        protected ctx: CanvasRenderingContext2D,
        protected imageParts: ImageParts) { }

    draw(x?: number, y?: number) {
        this.ctx.drawImage(...this.image, typeof x == 'undefined' ? this.x * this.w : x, typeof y == 'undefined' ? this.y * this.w : y, this.w, this.w)
    }

    clear() {
        this.ctx.clearRect(this.x * this.w, this.y * this.w, this.w, this.w)
    }

    isOver(item: Drawable) {
        return this.x == item.x && this.y == item.y
    }
}

class Meal extends Drawable {
    image: ImagePart
    constructor(
        public x: number,
        public y: number,
        protected w: number,
        protected ctx: CanvasRenderingContext2D,
        protected imageParts: ImageParts) {
        super(x, y, w, ctx, imageParts)
        this.image = imageParts.meal
    }
}

class SnakePart extends Drawable {
    next?: SnakePart
    prev?: SnakePart
    direction: Direction
    isEating = false

    get isHead() {
        return !this.next && !!this.prev
    }

    get isTail() {
        return !this.prev && !!this.next
    }

    get isBody() {
        return !!this.prev && !!this.next
    }

    get rotation() {
        if (this.isTurning) return 0;
        switch (this.direction) {
            case Direction.ArrowDown:
                return (Math.PI / 180) * 90
                break
            case Direction.ArrowLeft:
                return (Math.PI / 180) * 180
                break
            case Direction.ArrowUp:
                return (Math.PI / 180) * 270
                break;
        }
        return 0;
    }
    get isTurning() {
        return this.next && this.direction != this.next.direction
    }

    get image() {
        if (this.isEating) return this.imageParts.headEat;
        let turn = 0;
        if (this.isTurning) {
            switch (this.direction) {
                case Direction.ArrowRight:
                    turn = this.next.direction == Direction.ArrowDown ? 5 : 2
                    break
                case Direction.ArrowDown:
                    turn = this.next.direction == Direction.ArrowLeft ? 6 : 3
                    break
                case Direction.ArrowLeft:
                    turn = this.next.direction == Direction.ArrowUp ? 7 : 4
                    break
                case Direction.ArrowUp:
                    turn = this.next.direction == Direction.ArrowRight ? 8 : 1
                    break
            }
        }
        return this.isHead ? this.imageParts.head :
            (this.isTail ? this.imageParts.tail[turn] : this.imageParts.body[turn > 4 ? turn - 4 : turn])
    }

    translate() {
        this.ctx.translate(this.x * this.w, this.y * this.w)
        this.ctx.translate(this.w / 2, this.w / 2);
        this.ctx.rotate(this.rotation);
        this.ctx.translate(-(this.w / 2), -(this.w / 2));
    }

    restore() {
        this.ctx.translate(this.w / 2, this.w / 2);
        this.ctx.rotate(-this.rotation);
        this.ctx.translate(-(this.w / 2), -(this.w / 2));
        this.ctx.translate(-(this.x * this.w), -(this.y * this.w))
    }

    draw() {
        this.translate()
        super.draw(0, 0)
        this.restore()
    }

    move() {
        switch (this.direction) {
            case Direction.ArrowLeft:
                this.x--
                break;
            case Direction.ArrowRight:
                this.x++
                break;
            case Direction.ArrowUp:
                this.y--
                break;
            case Direction.ArrowDown:
                this.y++
                break;
        }
    }
    /** Grow tail */
    grow() {
        const newPart = new SnakePart(this.x, this.y, this.w, this.ctx, this.imageParts);
        newPart.direction = this.direction ^ 2 // Opposite direction
        newPart.move()
        newPart.direction = this.direction
        newPart.next = this
        return this.prev = newPart
    }
    clear() {
        super.clear()
        this.isEating = false;
    }
    toHead(cb: (v: SnakePart) => void) {
        let part: SnakePart = this;
        do {
            cb.call(part, part)
        } while (part = part.next)
    }

    toTail(cb: (v: SnakePart) => void) {
        let part: SnakePart = this;
        do {
            cb.call(part, part)
        } while (part = part.prev)
    }
}

class SnakeGame {
    speed = 3; // FPS
    direction: Direction
    ctx: CanvasRenderingContext2D
    boxSize: number
    timer: number
    meal: Meal
    head: SnakePart
    tail: SnakePart
    onDie?: (score: number) => void

    private _score: number

    set score(value: number) {
        this.scoreEl.innerText = (this._score = value).toString()
    }

    get score() {
        return this._score
    }

    dump() {
        this.head.toTail(p => console.log([p.x, p.y, Direction[p.direction]]))
    }
    constructor(
        public canvas: HTMLCanvasElement,
        public scoreEl: HTMLElement,
        public imageParts: ImageParts,
        public boxCountPerLine = 20
    ) {
        this.boxSize = canvas.width / boxCountPerLine
        this.ctx = this.canvas.getContext('2d');
        document.addEventListener('keydown', (e) => {
            if (e.code == 'Space') this.toggle()
            if (typeof Direction[e.code] != 'undefined') {
                // Tidak bisa balik
                if (Direction[e.code] % 2 != this.head.direction % 2) {
                    this.direction = Direction[e.code]
                }
            }
        })
        this.restart()
    }

    clear() {
        this.head.toTail(p => p.clear())
    }

    draw() {
        this.tail.toHead(part => part.draw())
        this.meal && this.meal.draw()
    }

    move() {
        this.tail.toHead(
            part => {
                part.direction = part.next ? part.next.direction : this.direction
            }
        )
        this.head.toTail(part => part.move())
    }

    placeMeal() {
        this.meal && this.meal.clear()
        do {
            this.meal = new Meal(
                Math.floor(Math.random() * this.boxCountPerLine),
                Math.floor(Math.random() * this.boxCountPerLine),
                this.boxSize, this.ctx, this.imageParts
            )
        } while (this.meal.isOver(this.head))
    }

    restart() {
        if (this.meal) {
            this.meal.clear()
        }
        this.score = 0;
        this.head = new SnakePart(2, Math.ceil(this.boxCountPerLine / 2), this.boxSize, this.ctx, this.imageParts)
        this.head.direction = this.direction = Direction.ArrowRight
        this.tail = this.head.grow().grow()
        this.draw()
    }

    frame = () => {
        this.clear()
        this.move()
        if (this.meal.isOver(this.head)) {
            console.log('GOT SCORE');
            this.tail = this.tail.grow()
            this.score++
            this.head.isEating = true;
            this.placeMeal()
        } else {
        }
        // Check head stuck at line
        if (this.head.x < 0
            || this.head.y < 0
            || this.head.x >= this.boxCountPerLine
            || this.head.y >= this.boxCountPerLine) {
            return this.die()
        }
        // check head cross it self
        let part = this.head;
        while (part = part.prev) {
            if (this.head.isOver(part)) return this.die()
        }
        this.draw()
    }

    play() {
        this.placeMeal()
        this.frame()
        this.timer = setInterval(this.frame, 1000 / this.speed)
    }

    pause() {
        clearInterval(this.timer)
        this.timer = null
    }

    toggle() {
        this.timer ? this.pause() : this.play()
    }

    die() {
        this.pause()
        this.onDie && this.onDie.call(this, this.score);
        this.restart()
    }

}

// Load images
const img = new Image();
img.onload = (ev) => {
    window.snake = new SnakeGame(
        document.getElementById('canvas') as HTMLCanvasElement,
        document.getElementById('score'),
        {
            head: [img, 200, 100, 100, 100],
            headEat: [img, 300, 100, 100, 100],
            meal: [img, 400, 100, 100, 100],
            tail: [
                [img, 0, 100, 100, 100], // Straight
                [img, 0, 0, 100, 100], // Turn Normal
                [img, 100, 0, 100, 100],
                [img, 200, 0, 100, 100],
                [img, 300, 0, 100, 100],
                [img, 0, 300, 100, 100], // Turn Flipped
                [img, 100, 300, 100, 100],
                [img, 200, 300, 100, 100],
                [img, 300, 300, 100, 100]
            ],
            body: [
                [img, 100, 100, 100, 100], // Straight
                [img, 0, 200, 100, 100], // Turn 90
                [img, 100, 200, 100, 100],
                [img, 200, 200, 100, 100],
                [img, 300, 200, 100, 100]
            ]
        });
    snake.onDie = (score) => alert(`It's done, your score: ${score}`)
}

img.src = "images.png";
