var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
var Direction;
(function (Direction) {
    Direction[Direction["ArrowRight"] = 0] = "ArrowRight";
    Direction[Direction["ArrowDown"] = 1] = "ArrowDown";
    Direction[Direction["ArrowLeft"] = 2] = "ArrowLeft";
    Direction[Direction["ArrowUp"] = 3] = "ArrowUp";
})(Direction || (Direction = {}));
var Drawable = /** @class */ (function () {
    function Drawable(x, y, w, ctx, imageParts) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.ctx = ctx;
        this.imageParts = imageParts;
    }
    Drawable.prototype.draw = function (x, y) {
        var _a;
        (_a = this.ctx).drawImage.apply(_a, __spreadArray(__spreadArray([], this.image), [typeof x == 'undefined' ? this.x * this.w : x, typeof y == 'undefined' ? this.y * this.w : y, this.w, this.w]));
    };
    Drawable.prototype.clear = function () {
        this.ctx.clearRect(this.x * this.w, this.y * this.w, this.w, this.w);
    };
    Drawable.prototype.isOver = function (item) {
        return this.x == item.x && this.y == item.y;
    };
    return Drawable;
}());
var Meal = /** @class */ (function (_super) {
    __extends(Meal, _super);
    function Meal(x, y, w, ctx, imageParts) {
        var _this = _super.call(this, x, y, w, ctx, imageParts) || this;
        _this.x = x;
        _this.y = y;
        _this.w = w;
        _this.ctx = ctx;
        _this.imageParts = imageParts;
        _this.image = imageParts.meal;
        return _this;
    }
    return Meal;
}(Drawable));
var SnakePart = /** @class */ (function (_super) {
    __extends(SnakePart, _super);
    function SnakePart() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.isEating = false;
        return _this;
    }
    Object.defineProperty(SnakePart.prototype, "isHead", {
        get: function () {
            return !this.next && !!this.prev;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SnakePart.prototype, "isTail", {
        get: function () {
            return !this.prev && !!this.next;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SnakePart.prototype, "isBody", {
        get: function () {
            return !!this.prev && !!this.next;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SnakePart.prototype, "rotation", {
        get: function () {
            if (this.isTurning)
                return 0;
            switch (this.direction) {
                case Direction.ArrowDown:
                    return (Math.PI / 180) * 90;
                    break;
                case Direction.ArrowLeft:
                    return (Math.PI / 180) * 180;
                    break;
                case Direction.ArrowUp:
                    return (Math.PI / 180) * 270;
                    break;
            }
            return 0;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SnakePart.prototype, "isTurning", {
        get: function () {
            return this.next && this.direction != this.next.direction;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SnakePart.prototype, "image", {
        get: function () {
            if (this.isEating)
                return this.imageParts.headEat;
            var turn = 0;
            if (this.isTurning) {
                switch (this.direction) {
                    case Direction.ArrowRight:
                        turn = this.next.direction == Direction.ArrowDown ? 5 : 2;
                        break;
                    case Direction.ArrowDown:
                        turn = this.next.direction == Direction.ArrowLeft ? 6 : 3;
                        break;
                    case Direction.ArrowLeft:
                        turn = this.next.direction == Direction.ArrowUp ? 7 : 4;
                        break;
                    case Direction.ArrowUp:
                        turn = this.next.direction == Direction.ArrowRight ? 8 : 1;
                        break;
                }
            }
            return this.isHead ? this.imageParts.head :
                (this.isTail ? this.imageParts.tail[turn] : this.imageParts.body[turn > 4 ? turn - 4 : turn]);
        },
        enumerable: false,
        configurable: true
    });
    SnakePart.prototype.translate = function () {
        this.ctx.translate(this.x * this.w, this.y * this.w);
        this.ctx.translate(this.w / 2, this.w / 2);
        this.ctx.rotate(this.rotation);
        this.ctx.translate(-(this.w / 2), -(this.w / 2));
    };
    SnakePart.prototype.restore = function () {
        this.ctx.translate(this.w / 2, this.w / 2);
        this.ctx.rotate(-this.rotation);
        this.ctx.translate(-(this.w / 2), -(this.w / 2));
        this.ctx.translate(-(this.x * this.w), -(this.y * this.w));
    };
    SnakePart.prototype.draw = function () {
        this.translate();
        _super.prototype.draw.call(this, 0, 0);
        this.restore();
    };
    SnakePart.prototype.move = function () {
        switch (this.direction) {
            case Direction.ArrowLeft:
                this.x--;
                break;
            case Direction.ArrowRight:
                this.x++;
                break;
            case Direction.ArrowUp:
                this.y--;
                break;
            case Direction.ArrowDown:
                this.y++;
                break;
        }
    };
    /** Grow tail */
    SnakePart.prototype.grow = function () {
        var newPart = new SnakePart(this.x, this.y, this.w, this.ctx, this.imageParts);
        newPart.direction = this.direction ^ 2; // Opposite direction
        newPart.move();
        newPart.direction = this.direction;
        newPart.next = this;
        return this.prev = newPart;
    };
    SnakePart.prototype.clear = function () {
        _super.prototype.clear.call(this);
        this.isEating = false;
    };
    SnakePart.prototype.toHead = function (cb) {
        var part = this;
        do {
            cb.call(part, part);
        } while (part = part.next);
    };
    SnakePart.prototype.toTail = function (cb) {
        var part = this;
        do {
            cb.call(part, part);
        } while (part = part.prev);
    };
    return SnakePart;
}(Drawable));
var SnakeGame = /** @class */ (function () {
    function SnakeGame(canvas, scoreEl, imageParts, boxCountPerLine) {
        var _this = this;
        if (boxCountPerLine === void 0) { boxCountPerLine = 20; }
        this.canvas = canvas;
        this.scoreEl = scoreEl;
        this.imageParts = imageParts;
        this.boxCountPerLine = boxCountPerLine;
        this.speed = 3; // FPS
        this.frame = function () {
            _this.clear();
            _this.move();
            if (_this.meal.isOver(_this.head)) {
                console.log('GOT SCORE');
                _this.tail = _this.tail.grow();
                _this.score++;
                _this.head.isEating = true;
                _this.placeMeal();
            }
            else {
            }
            // Check head stuck at line
            if (_this.head.x < 0
                || _this.head.y < 0
                || _this.head.x >= _this.boxCountPerLine
                || _this.head.y >= _this.boxCountPerLine) {
                return _this.die();
            }
            // check head cross it self
            var part = _this.head;
            while (part = part.prev) {
                if (_this.head.isOver(part))
                    return _this.die();
            }
            _this.draw();
        };
        this.boxSize = canvas.width / boxCountPerLine;
        this.ctx = this.canvas.getContext('2d');
        document.addEventListener('keydown', function (e) {
            if (e.code == 'Space')
                _this.toggle();
            if (typeof Direction[e.code] != 'undefined') {
                // Tidak bisa balik
                if (Direction[e.code] % 2 != _this.head.direction % 2) {
                    _this.direction = Direction[e.code];
                }
            }
        });
        this.restart();
    }
    Object.defineProperty(SnakeGame.prototype, "score", {
        get: function () {
            return this._score;
        },
        set: function (value) {
            this.scoreEl.innerText = (this._score = value).toString();
        },
        enumerable: false,
        configurable: true
    });
    SnakeGame.prototype.dump = function () {
        this.head.toTail(function (p) { return console.log([p.x, p.y, Direction[p.direction]]); });
    };
    SnakeGame.prototype.clear = function () {
        this.head.toTail(function (p) { return p.clear(); });
    };
    SnakeGame.prototype.draw = function () {
        this.tail.toHead(function (part) { return part.draw(); });
        this.meal && this.meal.draw();
    };
    SnakeGame.prototype.move = function () {
        var _this = this;
        this.tail.toHead(function (part) {
            part.direction = part.next ? part.next.direction : _this.direction;
        });
        this.head.toTail(function (part) { return part.move(); });
    };
    SnakeGame.prototype.placeMeal = function () {
        this.meal && this.meal.clear();
        do {
            this.meal = new Meal(Math.floor(Math.random() * this.boxCountPerLine), Math.floor(Math.random() * this.boxCountPerLine), this.boxSize, this.ctx, this.imageParts);
        } while (this.meal.isOver(this.head));
    };
    SnakeGame.prototype.restart = function () {
        if (this.meal) {
            this.meal.clear();
        }
        this.score = 0;
        this.head = new SnakePart(2, Math.ceil(this.boxCountPerLine / 2), this.boxSize, this.ctx, this.imageParts);
        this.head.direction = this.direction = Direction.ArrowRight;
        this.tail = this.head.grow().grow();
        this.draw();
    };
    SnakeGame.prototype.play = function () {
        this.placeMeal();
        this.frame();
        this.timer = setInterval(this.frame, 1000 / this.speed);
    };
    SnakeGame.prototype.pause = function () {
        clearInterval(this.timer);
        this.timer = null;
    };
    SnakeGame.prototype.toggle = function () {
        this.timer ? this.pause() : this.play();
    };
    SnakeGame.prototype.die = function () {
        this.pause();
        this.onDie && this.onDie.call(this, this.score);
        this.restart();
    };
    return SnakeGame;
}());
// Load images
var img = new Image();
img.onload = function (ev) {
    window.snake = new SnakeGame(document.getElementById('canvas'), document.getElementById('score'), {
        head: [img, 200, 100, 100, 100],
        headEat: [img, 300, 100, 100, 100],
        meal: [img, 400, 100, 100, 100],
        tail: [
            [img, 0, 100, 100, 100],
            [img, 0, 0, 100, 100],
            [img, 100, 0, 100, 100],
            [img, 200, 0, 100, 100],
            [img, 300, 0, 100, 100],
            [img, 0, 300, 100, 100],
            [img, 100, 300, 100, 100],
            [img, 200, 300, 100, 100],
            [img, 300, 300, 100, 100]
        ],
        body: [
            [img, 100, 100, 100, 100],
            [img, 0, 200, 100, 100],
            [img, 100, 200, 100, 100],
            [img, 200, 200, 100, 100],
            [img, 300, 200, 100, 100]
        ]
    });
    snake.onDie = function (score) { return alert("It's done, your score: " + score); };
};
img.src = "images.png";
